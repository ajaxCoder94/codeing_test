var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var companySchema = new Schema({
    address: {
        type: String,
        required:true
    },
    logo: {
        type: String,
        required:true
    },
    mobile: {
        type: String,
        required:true
    },
    name: {
        type: String,
        required:true
    },
    website: {
        type: String,
        required:true
    },
    location: {
        type: String,
        required:true
    },
    category: {
        type: String,
        required:true
    },
    tags: {
        type: Array
    },

    createdAt:{
        type:Date,
        default:new Date()
    }
});

// the schema is useless so far
// we need to create a model using it
var company = mongoose.model('Company', companySchema);

// make this available to our users in our Node applications
module.exports = company;