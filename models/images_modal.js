var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var imagesSchema = new Schema({
    base64String: {
        type: String
    },
    key: {
        type: String
    },
    date:{
        type: Date,
        default:new Date()
    },
    order:{
        type:Number
    },
    title:{
        type:String
    },
    htmlContent:{
        type:String,
        default:null
    },
    description:{
        type:String,
        default:null
    },
    mobile:{
        type:String,
        default:null
    },
    website:{
        type:String,
        default:null
    },
    address:{
        type:String,
        default:null
    },
    category:{
        type:String,
        default:null
    },
    icon:{
        type:String,
        default:null
    },
    video_link:{
        type:String,
        default:null
    }

});

// the schema is useless so far
// we need to create a model using it
var Images = mongoose.model('Images', imagesSchema);

// make this available to our users in our Node applications
module.exports = Images;