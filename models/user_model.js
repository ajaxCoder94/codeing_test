var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var userSchema = new Schema({
    company_name: {
        type: String
    },
    city: {
        type: String,
        required: [true, 'City is required']
    },
    first_name: {
        type: String,
        required: [true, 'First name is required']
    },
    last_name: {
        type: String,
        required: [true, 'Last name is required']
    },
    state:{
        type:String
    },
    zip:{
        type:Number
    },
    email:{
        type:String
    },
    web:{
        type:String
    },
    age:{
        type:Number,
        default:18
    }
});

// the schema is useless so far
// we need to create a model using it
var User = mongoose.model('User', userSchema);

// make this available to our users in our Node applications
module.exports = User;