const express = require('express');
const bodyParser = require('body-parser');
const app = express();
/*const MongoClient    = require('mongodb').MongoClient;*/
const db = require('./config/db');
var mongoose = require('mongoose');
const port = 8000;
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var cors = require('cors')
var apiRoutes = express.Router();
var url = require("url");
app.use(cors()) // Use this after the variable declaration
app.use(bodyParser.urlencoded({
    extended: true,
    limit: '50mb'
}));
app.set('superSecret', db.secret);
app.set('pass_hash',db.pass_hash);
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.use(bodyParser.json({limit: '50mb'}));


require('./app/routes')(app, {});

mongoose.connect(db.url, () => {
    console.log("mongo connect success");
    app.listen(port, () => {//remove ip if you wanna access localhost
        console.log('We are live on ' + port);
    });
});
