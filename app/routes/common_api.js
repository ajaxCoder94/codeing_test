var user = require('../../models/user_model')

module.exports = function (app) {
    app.put('/api/users/:id', (req, res) => {
        var id=req.params.id;
        user.update({'_id': id}, {$set: req.body}, (er, data) => {
            if (er) {
                res.status(400).send('Unable to update user.Please try later.')
            }
            else {
                res.send({msg: 'User updated successfully.'})
            }
        })
    })
    app.get('/api/users', (req, res) => {
        var query={};
        var sort_query={};
        if(req.query.page<=1){
            req.query.page=0;
        }
        var page = req.query.page ? req.query.page-1 : 0;
        var limit = req.query.limit ? req.query.limit : 500;
        if(req.query.name){
            /*query={
                'first'
            }*/
            query={
                '$or':[{'first_name':{'$regex':req.query.name}},{'last_name':{$regex:req.query.name}}]
            }
            console.log(query)
        }
        if(req.query.sort){
            var sorting_order=1;
            var order=req.query.sort.charAt(0);
            var sorting_key=req.query.sort;
            if(order=='-1'){
                sorting_order=-1;
            }
            sort_query[sorting_key]=sorting_order;
        }
        user.find(query).sort(sort_query).limit(limit).skip(page*limit, (e, u) => {
            res.send(u);
        })
    });
    app.get('/api/users/:id' , (req,res)=>{
        var id=req.params.id;
        console.log(id);
        user.find({'_id':id},(e,u)=>{
            res.send(u);
        })
    })
    app.post('/api/users', (req, res) => {
        var user1 = new user(req.body)
        user1.save(function (er, us) {
            if (er) {
                // if (er.code == 11000) {
                //     er = 'This E-mail is already taken'
                // }
                res.status(400).send(er);
            }
            else {
                res.status(201).send({msg: 'User registered successfully'});
            }
        })
    });
    app.delete('/api/users/:id',(req,res)=>{
        var id=req.params.id;
        user.delete({_id:id},(e,u)=>{
            if (e)
                res.status(400).send(er);
            else {
                res.status(201).send({msg: 'User registered successfully'});
            }
        })
    })
};