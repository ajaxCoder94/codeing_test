var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
module.exports={
    tokenVerifyFunc(app,token,cb){
        jwt.verify(token, app.get('superSecret'), function(err, decoded) {
            if (err) {
                cb(err)
            } else {
                // if everything is good, save to request for use in other routes
                console.log("sending")
                cb(null,decoded);
            }
        });
    }
}