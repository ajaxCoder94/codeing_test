var nodemailer = require('nodemailer');
const Cryptr = require('cryptr');

const options = {
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
        user: 'akshay.seth47@gmail.com',
        pass: 'akshayseth'
    }
};
const transporter = nodemailer.createTransport(options);
module.exports = {
    send_mail(app, to, subject, content, attachment, cb) {
        console.log("logging content");
        console.log(content)
        transporter.sendMail({
            from: "Delhi-Images-Support",
            to: to,
            // to: 'aseth200@gmail.com',
            subject: subject,
            //html:'Profile-'+user_data.profile+'<br>First Name-'+ user_data.firstname+ '<br>Last Name- '+user_data.lastname + '<br>Email- '+user_data.email + '<br>Contact Number-'+user_data.contact  + '<br>LinkedIN-'+user_data.linkedin,
            html: content,
            attachments: attachment ? {
                filename: attachment ? attachment.filename : null,
                content: attachment ? attachment.base64 : null,
                encoding: 'base64'
            } : null
        }, function (error, response) {
            if (error) {
                cb(error)
            }
            else {
                var new_res = {
                    "message": "Mail Sent"
                }
                cb(null, new_res)
            }
        });
    },
    encrypt_password(app,password,cb){
        console.log("coming in my helper")
        const cryptr = new Cryptr(app.get('pass_hash'));
        const encryptedString = cryptr.encrypt(password);
        cb(encryptedString);
    },
    decrypt_password(app,pass,cb){
        const cryptr = new Cryptr(app.get('pass_hash'));
        const decryptedString = cryptr.decrypt(pass);
        cb(decryptedString);

    },
    generate_activation_link(length) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < length; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    },
    getCountries() {
        return [
            {
                "countrycode": "AF",
                "countryflag": "https://restcountries.eu/data/afg.svg",
                "countryname": "Afghanistan",
                "phoneCode": "93"
            },
            {
                "countrycode": "AL ",
                "countryflag": "https://restcountries.eu/data/alb.svg",

                "countryname": "Albania",
                "phoneCode": "355"
            },
            {
                "countrycode": "DZ ",
                "countryflag": "https://restcountries.eu/data/dza.svg",

                "countryname": "Algeria",
                "phoneCode": "213"
            },
            {
                "countrycode": "AS ",
                "countryflag": "https://restcountries.eu/data/asm.svg",

                "countryname": "American Samoa",
                "phoneCode": "1-684"
            },
            {
                "countrycode": "AD ",
                "countryflag": "https://restcountries.eu/data/and.svg",

                "countryname": "Andorra",
                "phoneCode": "376"
            },
            {
                "countrycode": "AO ",
                "countryflag": "https://restcountries.eu/data/ago.svg",

                "countryname": "Angola",
                "phoneCode": "244"
            },
            {
                "countrycode": "AI ",
                "countryflag": "https://restcountries.eu/data/aia.svg",

                "countryname": "Anguilla",
                "phoneCode": "1-264"
            },
            {
                "countrycode": "AQ ",
                "countryflag": "https://restcountries.eu/data/ata.svg",

                "countryname": "Antarctica",
                "phoneCode": "672"
            },
            {
                "countrycode": "AG ",
                "countryflag": "https://restcountries.eu/data/atg.svg",

                "countryname": "Antigua and Barbuda",
                "phoneCode": "1-268"
            },
            {
                "countrycode": "AR ",
                "countryflag": "https://restcountries.eu/data/arg.svg",

                "countryname": "Argentina",
                "phoneCode": "54"
            },
            {
                "countrycode": "AM ",
                "countryflag": "https://restcountries.eu/data/arm.svg",

                "countryname": "Armenia",
                "phoneCode": "374"
            },
            {
                "countrycode": "AW ",
                "countryflag": "https://restcountries.eu/data/abw.svg",

                "countryname": "Aruba",
                "phoneCode": "297"
            },
            {
                "countrycode": "AU ",
                "countryflag": "https://restcountries.eu/data/aus.svg",

                "countryname": "Australia",
                "phoneCode": "61"
            },
            {
                "countrycode": "AT ",
                "countryflag": "https://restcountries.eu/data/aut.svg",

                "countryname": "Austria",
                "phoneCode": "43"
            },
            {
                "countrycode": "AZ ",
                "countryflag": "https://restcountries.eu/data/aze.svg",

                "countryname": "Azerbaijan",
                "phoneCode": "994"
            },
            {
                "countrycode": "BS ",
                "countryflag": "https://restcountries.eu/data/bhs.svg",

                "countryname": "Bahamas",
                "phoneCode": "1-242"
            },
            {
                "countrycode": "BH ",
                "countryflag": "https://restcountries.eu/data/bhr.svg",

                "countryname": "Bahrain",
                "phoneCode": "973"
            },
            {
                "countrycode": "BD ",
                "countryflag": "https://restcountries.eu/data/bgd.svg",

                "countryname": "Bangladesh",
                "phoneCode": "880"
            },
            {
                "countrycode": "BB ",
                "countryflag": "https://restcountries.eu/data/brb.svg",

                "countryname": "Barbados",
                "phoneCode": "1-246"
            },
            {
                "countrycode": "BY ",
                "countryflag": "https://restcountries.eu/data/blr.svg",

                "countryname": "Belarus",
                "phoneCode": "375"
            },
            {
                "countrycode": "BE ",
                "countryflag": "https://restcountries.eu/data/bel.svg",

                "countryname": "Belgium",
                "phoneCode": "32"
            },
            {
                "countrycode": "BZ ",
                "countryflag": "https://restcountries.eu/data/blz.svg",

                "countryname": "Belize",
                "phoneCode": "501"
            },
            {
                "countrycode": "BJ ",
                "countryflag": "https://restcountries.eu/data/ben.svg",

                "countryname": "Benin",
                "phoneCode": "229"
            },
            {
                "countrycode": "BM ",
                "countryflag": "https://restcountries.eu/data/bmu.svg",

                "countryname": "Bermuda",
                "phoneCode": "1-441"
            },
            {
                "countrycode": "BT ",
                "countryflag": "https://restcountries.eu/data/btn.svg",

                "countryname": "Bhutan",
                "phoneCode": "975"
            },
            {
                "countrycode": "BO ",
                "countryflag": "https://restcountries.eu/data/bol.svg",

                "countryname": "Bolivia",
                "phoneCode": "591"
            },
            {
                "countrycode": "BA ",
                "countryflag": "https://restcountries.eu/data/bih.svg",

                "countryname": "Bosnia and Herzegovina",
                "phoneCode": "387"
            },
            {
                "countrycode": "BW ",
                "countryflag": "https://restcountries.eu/data/bwa.svg",

                "countryname": "Botswana",
                "phoneCode": "267"
            },
            {
                "countrycode": "BR ",
                "countryflag": "https://restcountries.eu/data/bra.svg",

                "countryname": "Brazil",
                "phoneCode": "55"
            },
            {
                "countrycode": "IO ",
                "countryflag": "https://restcountries.eu/data/iot.svg",

                "countryname": "British Indian Ocean Territory",
                "phoneCode": "246"
            },
            {
                "countrycode": "VG ",
                "countryflag": "https://restcountries.eu/data/vgb.svg",

                "countryname": "British Virgin Islands",
                "phoneCode": "1-284"
            },
            {
                "countrycode": "BN ",
                "countryflag": "https://restcountries.eu/data/brn.svg",

                "countryname": "Brunei",
                "phoneCode": "673"
            },
            {
                "countrycode": "BG ",
                "countryflag": "https://restcountries.eu/data/bgr.svg",

                "countryname": "Bulgaria",
                "phoneCode": "359"
            },
            {
                "countrycode": "BF ",
                "countryflag": "https://restcountries.eu/data/bfa.svg",

                "countryname": "Burkina Faso",
                "phoneCode": "226"
            },
            {
                "countrycode": "BI ",
                "countryflag": "https://restcountries.eu/data/bdi.svg",

                "countryname": "Burundi",
                "phoneCode": "257"
            },
            {
                "countrycode": "KH ",
                "countryflag": "https://restcountries.eu/data/khm.svg",

                "countryname": "Cambodia",
                "phoneCode": "855"
            },
            {
                "countrycode": "CM ",
                "countryflag": "https://restcountries.eu/data/cmr.svg",

                "countryname": "Cameroon",
                "phoneCode": "237"
            },
            {
                "countrycode": "CA ",
                "countryflag": "https://restcountries.eu/data/can.svg",

                "countryname": "Canada",
                "phoneCode": "1"
            },
            {
                "countrycode": "CV ",
                "countryflag": "https://restcountries.eu/data/cpv.svg",

                "countryname": "Cape Verde",
                "phoneCode": "238"
            },
            {
                "countrycode": "KY ",
                "countryflag": "https://restcountries.eu/data/cym.svg",

                "countryname": "Cayman Islands",
                "phoneCode": "1-345"
            },
            {
                "countrycode": "CF ",
                "countryflag": "https://restcountries.eu/data/caf.svg",

                "countryname": "Central African Republic",
                "phoneCode": "236"
            },
            {
                "countrycode": "TD ",
                "countryflag": "https://restcountries.eu/data/tcd.svg",

                "countryname": "Chad",
                "phoneCode": "235"
            },
            {
                "countrycode": "CL ",
                "countryflag": "https://restcountries.eu/data/chl.svg",

                "countryname": "Chile",
                "phoneCode": "56"
            },
            {
                "countrycode": "CN ",
                "countryflag": "https://restcountries.eu/data/chn.svg",

                "countryname": "China",
                "phoneCode": "86"
            },
            {
                "countrycode": "CX ",
                "countryflag": "https://restcountries.eu/data/cxr.svg",

                "countryname": "Christmas Island",
                "phoneCode": "61"
            },
            {
                "countrycode": "CC ",
                "countryflag": "https://restcountries.eu/data/cck.svg",

                "countryname": "Cocos Islands",
                "phoneCode": "61"
            },
            {
                "countrycode": "CO ",
                "countryflag": "https://restcountries.eu/data/col.svg",

                "countryname": "Colombia",
                "phoneCode": "57"
            },
            {
                "countrycode": "KM ",
                "countryflag": "https://restcountries.eu/data/com.svg",

                "countryname": "Comoros",
                "phoneCode": "269"
            },
            {
                "countrycode": "CK ",
                "countryflag": "https://restcountries.eu/data/cok.svg",

                "countryname": "Cook Islands",
                "phoneCode": "682"
            },
            {
                "countrycode": "CR ",
                "countryflag": "https://restcountries.eu/data/cri.svg",

                "countryname": "Costa Rica",
                "phoneCode": "506"
            },
            {
                "countrycode": "HR ",
                "countryflag": "https://restcountries.eu/data/hrv.svg",

                "countryname": "Croatia",
                "phoneCode": "385"
            },
            {
                "countrycode": "CU ",
                "countryflag": "https://restcountries.eu/data/cub.svg",

                "countryname": "Cuba",
                "phoneCode": "53"
            },
            {
                "countrycode": "CW ",
                "countryflag": "https://restcountries.eu/data/cuw.svg",

                "countryname": "Curacao",
                "phoneCode": "599"
            },
            {
                "countrycode": "CY ",
                "countryflag": "https://restcountries.eu/data/cyp.svg",

                "countryname": "Cyprus",
                "phoneCode": "357"
            },
            {
                "countrycode": "CZ ",
                "countryflag": "https://restcountries.eu/data/cze.svg",

                "countryname": "Czech Republic",
                "phoneCode": "420"
            },
            {
                "countrycode": "CD ",
                "countryflag": "https://restcountries.eu/data/cod.svg",

                "countryname": "Democratic Republic of the Congo",
                "phoneCode": "243"
            },
            {
                "countrycode": "DK ",
                "countryflag": "https://restcountries.eu/data/dnk.svg",

                "countryname": "Denmark",
                "phoneCode": "45"
            },
            {
                "countrycode": "DJ ",
                "countryflag": "https://restcountries.eu/data/dji.svg",

                "countryname": "Djibouti",
                "phoneCode": "253"
            },
            {
                "countrycode": "DM ",
                "countryflag": "https://restcountries.eu/data/dma.svg",

                "countryname": "Dominica",
                "phoneCode": "1-767"
            },
            {
                "countrycode": "DO ",
                "countryflag": "https://restcountries.eu/data/dom.svg",

                "countryname": "Dominican Republic",
                "phoneCode": "1-809, 1-829, 1-849"
            },
            {
                "countrycode": "TL ",
                "countryflag": "https://restcountries.eu/data/tls.svg",

                "countryname": "East Timor",
                "phoneCode": "670"
            },
            {
                "countrycode": "EC ",
                "countryflag": "https://restcountries.eu/data/ecu.svg",

                "countryname": "Ecuador",
                "phoneCode": "593"
            },
            {
                "countrycode": "EG ",
                "countryflag": "https://restcountries.eu/data/egy.svg",

                "countryname": "Egypt",
                "phoneCode": "20"
            },
            {
                "countrycode": "SV ",
                "countryflag": "https://restcountries.eu/data/slv.svg",

                "countryname": "El Salvador",
                "phoneCode": "503"
            },
            {
                "countrycode": "GQ ",
                "countryflag": "https://restcountries.eu/data/gnq.svg",

                "countryname": "Equatorial Guinea",
                "phoneCode": "240"
            },
            {
                "countrycode": "ER ",
                "countryflag": "https://restcountries.eu/data/eri.svg",

                "countryname": "Eritrea",
                "phoneCode": "291"
            },
            {
                "countrycode": "EE ",
                "countryflag": "https://restcountries.eu/data/est.svg",

                "countryname": "Estonia",
                "phoneCode": "372"
            },
            {
                "countrycode": "ET ",
                "countryflag": "https://restcountries.eu/data/eth.svg",

                "countryname": "Ethiopia",
                "phoneCode": "251"
            },
            {
                "countrycode": "FK ",
                "countryflag": "https://restcountries.eu/data/flk.svg",

                "countryname": "Falkland Islands",
                "phoneCode": "500"
            },
            {
                "countrycode": "FO ",
                "countryflag": "https://restcountries.eu/data/fro.svg",

                "countryname": "Faroe Islands",
                "phoneCode": "298"
            },
            {
                "countrycode": "FJ ",
                "countryflag": "https://restcountries.eu/data/fji.svg",

                "countryname": "Fiji",
                "phoneCode": "679"
            },
            {
                "countrycode": "FI ",
                "countryflag": "https://restcountries.eu/data/fin.svg",

                "countryname": "Finland",
                "phoneCode": "358"
            },
            {
                "countrycode": "FR ",
                "countryflag": "https://restcountries.eu/data/fra.svg",

                "countryname": "France",
                "phoneCode": "33"
            },
            {
                "countrycode": "PF ",
                "countryflag": "https://restcountries.eu/data/pyf.svg",

                "countryname": "French Polynesia",
                "phoneCode": "689"
            },
            {
                "countrycode": "GA ",
                "countryflag": "https://restcountries.eu/data/gab.svg",

                "countryname": "Gabon",
                "phoneCode": "241"
            },
            {
                "countrycode": "GM ",
                "countryflag": "https://restcountries.eu/data/gmb.svg",

                "countryname": "Gambia",
                "phoneCode": "220"
            },
            {
                "countrycode": "GE ",
                "countryflag": "https://restcountries.eu/data/geo.svg",

                "countryname": "Georgia",
                "phoneCode": "995"
            },
            {
                "countrycode": "DE ",
                "countryflag": "https://restcountries.eu/data/deu.svg",

                "countryname": "Germany",
                "phoneCode": "49"
            },
            {
                "countrycode": "GH ",
                "countryflag": "https://restcountries.eu/data/gha.svg",

                "countryname": "Ghana",
                "phoneCode": "233"
            },
            {
                "countrycode": "GI ",
                "countryflag": "https://restcountries.eu/data/gib.svg",

                "countryname": "Gibraltar",
                "phoneCode": "350"
            },
            {
                "countrycode": "GR ",
                "countryflag": "https://restcountries.eu/data/grc.svg",

                "countryname": "Greece",
                "phoneCode": "30"
            },
            {
                "countrycode": "GL ",
                "countryflag": "https://restcountries.eu/data/grl.svg",

                "countryname": "Greenland",
                "phoneCode": "299"
            },
            {
                "countrycode": "GD ",
                "countryflag": "https://restcountries.eu/data/grd.svg",

                "countryname": "Grenada",
                "phoneCode": "1-473"
            },
            {
                "countrycode": "GU ",
                "countryflag": "https://restcountries.eu/data/gum.svg",

                "countryname": "Guam",
                "phoneCode": "1-671"
            },
            {
                "countrycode": "GT ",
                "countryflag": "https://restcountries.eu/data/gtm.svg",

                "countryname": "Guatemala",
                "phoneCode": "502"
            },
            {
                "countrycode": "GG ",
                "countryflag": "https://restcountries.eu/data/ggy.svg",

                "countryname": "Guernsey",
                "phoneCode": "44-1481"
            },
            {
                "countrycode": "GN ",
                "countryflag": "https://restcountries.eu/data/gin.svg",

                "countryname": "Guinea",
                "phoneCode": "224"
            },
            {
                "countrycode": "GW ",
                "countryflag": "https://restcountries.eu/data/gnb.svg",

                "countryname": "Guinea-Bissau",
                "phoneCode": "245"
            },
            {
                "countrycode": "GY ",
                "countryflag": "https://restcountries.eu/data/guy.svg",

                "countryname": "Guyana",
                "phoneCode": "592"
            },
            {
                "countrycode": "HT ",
                "countryflag": "https://restcountries.eu/data/hti.svg",

                "countryname": "Haiti",
                "phoneCode": "509"
            },
            {
                "countrycode": "HN ",
                "countryflag": "https://restcountries.eu/data/hnd.svg",

                "countryname": "Honduras",
                "phoneCode": "504"
            },
            {
                "countrycode": "HK ",
                "countryflag": "https://restcountries.eu/data/hkg.svg",

                "countryname": "Hong Kong",
                "phoneCode": "852"
            },
            {
                "countrycode": "HU ",
                "countryflag": "https://restcountries.eu/data/hun.svg",

                "countryname": "Hungary",
                "phoneCode": "36"
            },
            {
                "countrycode": "IS ",
                "countryflag": "https://restcountries.eu/data/isl.svg",

                "countryname": "Iceland",
                "phoneCode": "354"
            },
            {
                "countrycode": "IN ",
                "countryflag": "https://restcountries.eu/data/ind.svg",

                "countryname": "India",
                "phoneCode": "91"
            },
            {
                "countrycode": "ID ",
                "countryflag": "https://restcountries.eu/data/idn.svg",

                "countryname": "Indonesia",
                "phoneCode": "62"
            },
            {
                "countrycode": "IR ",
                "countryflag": "https://restcountries.eu/data/irn.svg",

                "countryname": "Iran",
                "phoneCode": "98"
            },
            {
                "countrycode": "IQ ",
                "countryflag": "https://restcountries.eu/data/irq.svg",

                "countryname": "Iraq",
                "phoneCode": "964"
            },
            {
                "countrycode": "IE ",
                "countryflag": "https://restcountries.eu/data/irl.svg",

                "countryname": "Ireland",
                "phoneCode": "353"
            },
            {
                "countrycode": "IM ",
                "countryflag": "https://restcountries.eu/data/imn.svg",

                "countryname": "Isle of Man",
                "phoneCode": "44-1624"
            },
            {
                "countrycode": "IL ",
                "countryflag": "https://restcountries.eu/data/isr.svg",

                "countryname": "Israel",
                "phoneCode": "972"
            },
            {
                "countrycode": "IT ",
                "countryflag": "https://restcountries.eu/data/ita.svg",

                "countryname": "Italy",
                "phoneCode": "39"
            },
            {
                "countrycode": "CI ",
                "countryflag": "https://restcountries.eu/data/civ.svg",

                "countryname": "Ivory Coast",
                "phoneCode": "225"
            },
            {
                "countrycode": "JM ",
                "countryflag": "https://restcountries.eu/data/jam.svg",

                "countryname": "Jamaica",
                "phoneCode": "1-876"
            },
            {
                "countrycode": "JP ",
                "countryflag": "https://restcountries.eu/data/jpn.svg",

                "countryname": "Japan",
                "phoneCode": "81"
            },
            {
                "countrycode": "JE ",
                "countryflag": "https://restcountries.eu/data/jey.svg",

                "countryname": "Jersey",
                "phoneCode": "44-1534"
            },
            {
                "countrycode": "JO ",
                "countryflag": "https://restcountries.eu/data/jor.svg",

                "countryname": "Jordan",
                "phoneCode": "962"
            },
            {
                "countrycode": "KZ ",
                "countryflag": "https://restcountries.eu/data/kaz.svg",

                "countryname": "Kazakhstan",
                "phoneCode": "7"
            },
            {
                "countrycode": "KE ",
                "countryflag": "https://restcountries.eu/data/ken.svg",

                "countryname": "Kenya",
                "phoneCode": "254"
            },
            {
                "countrycode": "KI ",
                "countryflag": "https://restcountries.eu/data/kir.svg",

                "countryname": "Kiribati",
                "phoneCode": "686"
            },
            {
                "countrycode": "XK ",
                "countryflag": "https://restcountries.eu/data/xkx.svg",

                "countryname": "Kosovo",
                "phoneCode": "383"
            },
            {
                "countrycode": "KW ",
                "countryflag": "https://restcountries.eu/data/kwt.svg",

                "countryname": "Kuwait",
                "phoneCode": "965"
            },
            {
                "countrycode": "KG ",
                "countryflag": "https://restcountries.eu/data/kgz.svg",

                "countryname": "Kyrgyzstan",
                "phoneCode": "996"
            },
            {
                "countrycode": "LA ",
                "countryflag": "https://restcountries.eu/data/lao.svg",

                "countryname": "Laos",
                "phoneCode": "856"
            },
            {
                "countrycode": "LV ",
                "countryflag": "https://restcountries.eu/data/lva.svg",

                "countryname": "Latvia",
                "phoneCode": "371"
            },
            {
                "countrycode": "LB ",
                "countryflag": "https://restcountries.eu/data/lbn.svg",

                "countryname": "Lebanon",
                "phoneCode": "961"
            },
            {
                "countrycode": "LS ",
                "countryflag": "https://restcountries.eu/data/lso.svg",

                "countryname": "Lesotho",
                "phoneCode": "266"
            },
            {
                "countrycode": "LR ",
                "countryflag": "https://restcountries.eu/data/lbr.svg",

                "countryname": "Liberia",
                "phoneCode": "231"
            },
            {
                "countrycode": "LY ",
                "countryflag": "https://restcountries.eu/data/lby.svg",

                "countryname": "Libya",
                "phoneCode": "218"
            },
            {
                "countrycode": "LI ",
                "countryflag": "https://restcountries.eu/data/lie.svg",

                "countryname": "Liechtenstein",
                "phoneCode": "423"
            },
            {
                "countrycode": "LT ",
                "countryflag": "https://restcountries.eu/data/ltu.svg",

                "countryname": "Lithuania",
                "phoneCode": "370"
            },
            {
                "countrycode": "LU ",
                "countryflag": "https://restcountries.eu/data/lux.svg",

                "countryname": "Luxembourg",
                "phoneCode": "352"
            },
            {
                "countrycode": "MO ",
                "countryflag": "https://restcountries.eu/data/mac.svg",

                "countryname": "Macau",
                "phoneCode": "853"
            },
            {
                "countrycode": "MK ",
                "countryflag": "https://restcountries.eu/data/mkd.svg",

                "countryname": "Macedonia",
                "phoneCode": "389"
            },
            {
                "countrycode": "MG ",
                "countryflag": "https://restcountries.eu/data/mdg.svg",

                "countryname": "Madagascar",
                "phoneCode": "261"
            },
            {
                "countrycode": "MW ",
                "countryflag": "https://restcountries.eu/data/mwi.svg",

                "countryname": "Malawi",
                "phoneCode": "265"
            },
            {
                "countrycode": "MY ",
                "countryflag": "https://restcountries.eu/data/mys.svg",

                "countryname": "Malaysia",
                "phoneCode": "60"
            },
            {
                "countrycode": "MV ",
                "countryflag": "https://restcountries.eu/data/mdv.svg",

                "countryname": "Maldives",
                "phoneCode": "960"
            },
            {
                "countrycode": "ML ",
                "countryflag": "https://restcountries.eu/data/mli.svg",

                "countryname": "Mali",
                "phoneCode": "223"
            },
            {
                "countrycode": "MT ",
                "countryflag": "https://restcountries.eu/data/mlt.svg",

                "countryname": "Malta",
                "phoneCode": "356"
            },
            {
                "countrycode": "MH ",
                "countryflag": "https://restcountries.eu/data/mhl.svg",

                "countryname": "Marshall Islands",
                "phoneCode": "692"
            },
            {
                "countrycode": "MR ",
                "countryflag": "https://restcountries.eu/data/mrt.svg",

                "countryname": "Mauritania",
                "phoneCode": "222"
            },
            {
                "countrycode": "MU ",
                "countryflag": "https://restcountries.eu/data/mus.svg",

                "countryname": "Mauritius",
                "phoneCode": "230"
            },
            {
                "countrycode": "YT ",
                "countryflag": "https://restcountries.eu/data/myt.svg",

                "countryname": "Mayotte",
                "phoneCode": "262"
            },
            {
                "countrycode": "MX ",
                "countryflag": "https://restcountries.eu/data/mex.svg",

                "countryname": "Mexico",
                "phoneCode": "52"
            },
            {
                "countrycode": "FM ",
                "countryflag": "https://restcountries.eu/data/fsm.svg",

                "countryname": "Micronesia",
                "phoneCode": "691"
            },
            {
                "countrycode": "MD ",
                "countryflag": "https://restcountries.eu/data/mda.svg",

                "countryname": "Moldova",
                "phoneCode": "373"
            },
            {
                "countrycode": "MC ",
                "countryflag": "https://restcountries.eu/data/mco.svg",

                "countryname": "Monaco",
                "phoneCode": "377"
            },
            {
                "countrycode": "MN ",
                "countryflag": "https://restcountries.eu/data/mng.svg",

                "countryname": "Mongolia",
                "phoneCode": "976"
            },
            {
                "countrycode": "ME ",
                "countryflag": "https://restcountries.eu/data/mne.svg",

                "countryname": "Montenegro",
                "phoneCode": "382"
            },
            {
                "countrycode": "MS ",
                "countryflag": "https://restcountries.eu/data/msr.svg",

                "countryname": "Montserrat",
                "phoneCode": "1-664"
            },
            {
                "countrycode": "MA ",
                "countryflag": "https://restcountries.eu/data/mar.svg",

                "countryname": "Morocco",
                "phoneCode": "212"
            },
            {
                "countrycode": "MZ ",
                "countryflag": "https://restcountries.eu/data/moz.svg",

                "countryname": "Mozambique",
                "phoneCode": "258"
            },
            {
                "countrycode": "MM ",
                "countryflag": "https://restcountries.eu/data/mmr.svg",

                "countryname": "Myanmar",
                "phoneCode": "95"
            },
            {
                "countrycode": "NA ",
                "countryflag": "https://restcountries.eu/data/nam.svg",

                "countryname": "Namibia",
                "phoneCode": "264"
            },
            {
                "countrycode": "NR ",
                "countryflag": "https://restcountries.eu/data/nru.svg",

                "countryname": "Nauru",
                "phoneCode": "674"
            },
            {
                "countrycode": "NP ",
                "countryflag": "https://restcountries.eu/data/npl.svg",

                "countryname": "Nepal",
                "phoneCode": "977"
            },
            {
                "countrycode": "NL ",
                "countryflag": "https://restcountries.eu/data/nld.svg",

                "countryname": "Netherlands",
                "phoneCode": "31"
            },
            {
                "countrycode": "AN ",
                "countryflag": "https://restcountries.eu/data/ant.svg",

                "countryname": "Netherlands Antilles",
                "phoneCode": "599"
            },
            {
                "countrycode": "NC ",
                "countryflag": "https://restcountries.eu/data/ncl.svg",

                "countryname": "New Caledonia",
                "phoneCode": "687"
            },
            {
                "countrycode": "NZ ",
                "countryflag": "https://restcountries.eu/data/nzl.svg",

                "countryname": "New Zealand",
                "phoneCode": "64"
            },
            {
                "countrycode": "NI ",
                "countryflag": "https://restcountries.eu/data/nic.svg",

                "countryname": "Nicaragua",
                "phoneCode": "505"
            },
            {
                "countrycode": "NE ",
                "countryflag": "https://restcountries.eu/data/ner.svg",

                "countryname": "Niger",
                "phoneCode": "227"
            },
            {
                "countrycode": "NG ",
                "countryflag": "https://restcountries.eu/data/nga.svg",

                "countryname": "Nigeria",
                "phoneCode": "234"
            },
            {
                "countrycode": "NU ",
                "countryflag": "https://restcountries.eu/data/niu.svg",

                "countryname": "Niue",
                "phoneCode": "683"
            },
            {
                "countrycode": "KP ",
                "countryflag": "https://restcountries.eu/data/prk.svg",

                "countryname": "North Korea",
                "phoneCode": "850"
            },
            {
                "countrycode": "MP ",
                "countryflag": "https://restcountries.eu/data/mnp.svg",

                "countryname": "Northern Mariana Islands",
                "phoneCode": "1-670"
            },
            {
                "countrycode": "NO ",
                "countryflag": "https://restcountries.eu/data/nor.svg",

                "countryname": "Norway",
                "phoneCode": "47"
            },
            {
                "countrycode": "OM ",
                "countryflag": "https://restcountries.eu/data/omn.svg",

                "countryname": "Oman",
                "phoneCode": "968"
            },
            {
                "countrycode": "PK ",
                "countryflag": "https://restcountries.eu/data/pak.svg",

                "countryname": "Pakistan",
                "phoneCode": "92"
            },
            {
                "countrycode": "PW ",
                "countryflag": "https://restcountries.eu/data/plw.svg",

                "countryname": "Palau",
                "phoneCode": "680"
            },
            {
                "countrycode": "PS ",
                "countryflag": "https://restcountries.eu/data/pse.svg",

                "countryname": "Palestine",
                "phoneCode": "970"
            },
            {
                "countrycode": "PA ",
                "countryflag": "https://restcountries.eu/data/pan.svg",

                "countryname": "Panama",
                "phoneCode": "507"
            },
            {
                "countrycode": "PG ",
                "countryflag": "https://restcountries.eu/data/png.svg",

                "countryname": "Papua New Guinea",
                "phoneCode": "675"
            },
            {
                "countrycode": "PY ",
                "countryflag": "https://restcountries.eu/data/pry.svg",

                "countryname": "Paraguay",
                "phoneCode": "595"
            },
            {
                "countrycode": "PE ",
                "countryflag": "https://restcountries.eu/data/per.svg",

                "countryname": "Peru",
                "phoneCode": "51"
            },
            {
                "countrycode": "PH ",
                "countryflag": "https://restcountries.eu/data/phl.svg",

                "countryname": "Philippines",
                "phoneCode": "63"
            },
            {
                "countrycode": "PN ",
                "countryflag": "https://restcountries.eu/data/pcn.svg",

                "countryname": "Pitcairn",
                "phoneCode": "64"
            },
            {
                "countrycode": "PL ",
                "countryflag": "https://restcountries.eu/data/pol.svg",

                "countryname": "Poland",
                "phoneCode": "48"
            },
            {
                "countrycode": "PT ",
                "countryflag": "https://restcountries.eu/data/prt.svg",

                "countryname": "Portugal",
                "phoneCode": "351"
            },
            {
                "countrycode": "PR ",
                "countryflag": "https://restcountries.eu/data/pri.svg",

                "countryname": "Puerto Rico",
                "phoneCode": "1-787, 1-939"
            },
            {
                "countrycode": "QA ",
                "countryflag": "https://restcountries.eu/data/qat.svg",

                "countryname": "Qatar",
                "phoneCode": "974"
            },
            {
                "countrycode": "CG ",
                "countryflag": "https://restcountries.eu/data/cog.svg",

                "countryname": "Republic of the Congo",
                "phoneCode": "242"
            },
            {
                "countrycode": "RE ",
                "countryflag": "https://restcountries.eu/data/reu.svg",

                "countryname": "Reunion",
                "phoneCode": "262"
            },
            {
                "countrycode": "RO ",
                "countryflag": "https://restcountries.eu/data/rou.svg",

                "countryname": "Romania",
                "phoneCode": "40"
            },
            {
                "countrycode": "RU ",
                "countryflag": "https://restcountries.eu/data/rus.svg",

                "countryname": "Russia",
                "phoneCode": "7"
            },
            {
                "countrycode": "RW ",
                "countryflag": "https://restcountries.eu/data/rwa.svg",

                "countryname": "Rwanda",
                "phoneCode": "250"
            },
            {
                "countrycode": "BL ",
                "countryflag": "https://restcountries.eu/data/blm.svg",

                "countryname": "Saint Barthelemy",
                "phoneCode": "590"
            },
            {
                "countrycode": "SH ",
                "countryflag": "https://restcountries.eu/data/shn.svg",

                "countryname": "Saint Helena",
                "phoneCode": "290"
            },
            {
                "countrycode": "KN ",
                "countryflag": "https://restcountries.eu/data/kna.svg",

                "countryname": "Saint Kitts and Nevis",
                "phoneCode": "1-869"
            },
            {
                "countrycode": "LC ",
                "countryflag": "https://restcountries.eu/data/lca.svg",

                "countryname": "Saint Lucia",
                "phoneCode": "1-758"
            },
            {
                "countrycode": "MF ",
                "countryflag": "https://restcountries.eu/data/maf.svg",

                "countryname": "Saint Martin",
                "phoneCode": "590"
            },
            {
                "countrycode": "PM ",
                "countryflag": "https://restcountries.eu/data/spm.svg",

                "countryname": "Saint Pierre and Miquelon",
                "phoneCode": "508"
            },
            {
                "countrycode": "VC ",
                "countryflag": "https://restcountries.eu/data/vct.svg",

                "countryname": "Saint Vincent and the Grenadines",
                "phoneCode": "1-784"
            },
            {
                "countrycode": "WS ",
                "countryflag": "https://restcountries.eu/data/wsm.svg",

                "countryname": "Samoa",
                "phoneCode": "685"
            },
            {
                "countrycode": "SM ",
                "countryflag": "https://restcountries.eu/data/smr.svg",

                "countryname": "San Marino",
                "phoneCode": "378"
            },
            {
                "countrycode": "ST ",
                "countryflag": "https://restcountries.eu/data/stp.svg",

                "countryname": "Sao Tome and Principe",
                "phoneCode": "239"
            },
            {
                "countrycode": "SA ",
                "countryflag": "https://restcountries.eu/data/sau.svg",

                "countryname": "Saudi Arabia",
                "phoneCode": "966"
            },
            {
                "countrycode": "SN ",
                "countryflag": "https://restcountries.eu/data/sen.svg",

                "countryname": "Senegal",
                "phoneCode": "221"
            },
            {
                "countrycode": "RS ",
                "countryflag": "https://restcountries.eu/data/srb.svg",

                "countryname": "Serbia",
                "phoneCode": "381"
            },
            {
                "countrycode": "SC ",
                "countryflag": "https://restcountries.eu/data/syc.svg",

                "countryname": "Seychelles",
                "phoneCode": "248"
            },
            {
                "countrycode": "SL ",
                "countryflag": "https://restcountries.eu/data/sle.svg",

                "countryname": "Sierra Leone",
                "phoneCode": "232"
            },
            {
                "countrycode": "SG ",
                "countryflag": "https://restcountries.eu/data/sgp.svg",

                "countryname": "Singapore",
                "phoneCode": "65"
            },
            {
                "countrycode": "SX ",
                "countryflag": "https://restcountries.eu/data/sxm.svg",

                "countryname": "Sint Maarten",
                "phoneCode": "1-721"
            },
            {
                "countrycode": "SK ",
                "countryflag": "https://restcountries.eu/data/svk.svg",

                "countryname": "Slovakia",
                "phoneCode": "421"
            },
            {
                "countrycode": "SI ",
                "countryflag": "https://restcountries.eu/data/svn.svg",

                "countryname": "Slovenia",
                "phoneCode": "386"
            },
            {
                "countrycode": "SB ",
                "countryflag": "https://restcountries.eu/data/slb.svg",

                "countryname": "Solomon Islands",
                "phoneCode": "677"
            },
            {
                "countrycode": "SO ",
                "countryflag": "https://restcountries.eu/data/som.svg",

                "countryname": "Somalia",
                "phoneCode": "252"
            },
            {
                "countrycode": "ZA ",
                "countryflag": "https://restcountries.eu/data/zaf.svg",

                "countryname": "South Africa",
                "phoneCode": "27"
            },
            {
                "countrycode": "KR ",
                "countryflag": "https://restcountries.eu/data/kor.svg",

                "countryname": "South Korea",
                "phoneCode": "82"
            },
            {
                "countrycode": "SS ",
                "countryflag": "https://restcountries.eu/data/ssd.svg",

                "countryname": "South Sudan",
                "phoneCode": "211"
            },
            {
                "countrycode": "ES ",
                "countryflag": "https://restcountries.eu/data/esp.svg",

                "countryname": "Spain",
                "phoneCode": "34"
            },
            {
                "countrycode": "LK ",
                "countryflag": "https://restcountries.eu/data/lka.svg",

                "countryname": "Sri Lanka",
                "phoneCode": "94"
            },
            {
                "countrycode": "SD ",
                "countryflag": "https://restcountries.eu/data/sdn.svg",

                "countryname": "Sudan",
                "phoneCode": "249"
            },
            {
                "countrycode": "SR ",
                "countryflag": "https://restcountries.eu/data/sur.svg",

                "countryname": "Suriname",
                "phoneCode": "597"
            },
            {
                "countrycode": "SJ ",
                "countryflag": "https://restcountries.eu/data/sjm.svg",

                "countryname": "Svalbard and Jan Mayen",
                "phoneCode": "47"
            },
            {
                "countrycode": "SZ ",
                "countryflag": "https://restcountries.eu/data/swz.svg",

                "countryname": "Swaziland",
                "phoneCode": "268"
            },
            {
                "countrycode": "SE ",
                "countryflag": "https://restcountries.eu/data/swe.svg",

                "countryname": "Sweden",
                "phoneCode": "46"
            },
            {
                "countrycode": "CH ",
                "countryflag": "https://restcountries.eu/data/che.svg",

                "countryname": "Switzerland",
                "phoneCode": "41"
            },
            {
                "countrycode": "SY ",
                "countryflag": "https://restcountries.eu/data/syr.svg",

                "countryname": "Syria",
                "phoneCode": "963"
            },
            {
                "countrycode": "TW ",
                "countryflag": "https://restcountries.eu/data/twn.svg",

                "countryname": "Taiwan",
                "phoneCode": "886"
            },
            {
                "countrycode": "TJ ",
                "countryflag": "https://restcountries.eu/data/tjk.svg",

                "countryname": "Tajikistan",
                "phoneCode": "992"
            },
            {
                "countrycode": "TZ ",
                "countryflag": "https://restcountries.eu/data/tza.svg",

                "countryname": "Tanzania",
                "phoneCode": "255"
            },
            {
                "countrycode": "AF1",
                "countryflag": null,

                "countryname": "test",
                "phoneCode": "934"
            },
            {
                "countrycode": "AF1",
                "countryflag": null,

                "countryname": "test4",
                "phoneCode": "934"
            },
            {
                "countrycode": "TH ",
                "countryflag": "https://restcountries.eu/data/tha.svg",

                "countryname": "Thailand",
                "phoneCode": "66"
            },
            {
                "countrycode": "TG ",
                "countryflag": "https://restcountries.eu/data/tgo.svg",

                "countryname": "Togo",
                "phoneCode": "228"
            },
            {
                "countrycode": "TK ",
                "countryflag": "https://restcountries.eu/data/tkl.svg",

                "countryname": "Tokelau",
                "phoneCode": "690"
            },
            {
                "countrycode": "TO ",
                "countryflag": "https://restcountries.eu/data/ton.svg",

                "countryname": "Tonga",
                "phoneCode": "676"
            },
            {
                "countrycode": "TT ",
                "countryflag": "https://restcountries.eu/data/tto.svg",

                "countryname": "Trinidad and Tobago",
                "phoneCode": "1-868"
            },
            {
                "countrycode": "TN ",
                "countryflag": "https://restcountries.eu/data/tun.svg",

                "countryname": "Tunisia",
                "phoneCode": "216"
            },
            {
                "countrycode": "TR ",
                "countryflag": "https://restcountries.eu/data/tur.svg",

                "countryname": "Turkey",
                "phoneCode": "90"
            },
            {
                "countrycode": "TM ",
                "countryflag": "https://restcountries.eu/data/tkm.svg",

                "countryname": "Turkmenistan",
                "phoneCode": "993"
            },
            {
                "countrycode": "TC ",
                "countryflag": "https://restcountries.eu/data/tca.svg",

                "countryname": "Turks and Caicos Islands",
                "phoneCode": "1-649"
            },
            {
                "countrycode": "TV ",
                "countryflag": "https://restcountries.eu/data/tuv.svg",

                "countryname": "Tuvalu",
                "phoneCode": "688"
            },
            {
                "countrycode": "VI ",
                "countryflag": "https://restcountries.eu/data/vir.svg",

                "countryname": "U.S. Virgin Islands",
                "phoneCode": "1-340"
            },
            {
                "countrycode": "UG ",
                "countryflag": "https://restcountries.eu/data/uga.svg",

                "countryname": "Uganda",
                "phoneCode": "256"
            },
            {
                "countrycode": "UA ",
                "countryflag": "https://restcountries.eu/data/ukr.svg",

                "countryname": "Ukraine",
                "phoneCode": "380"
            },
            {
                "countrycode": "AE ",
                "countryflag": "https://restcountries.eu/data/are.svg",

                "countryname": "United Arab Emirates",
                "phoneCode": "971"
            },
            {
                "countrycode": "GB ",
                "countryflag": "https://restcountries.eu/data/gbr.svg",

                "countryname": "United Kingdom",
                "phoneCode": "44"
            },
            {
                "countrycode": "US ",
                "countryflag": "https://restcountries.eu/data/usa.svg",

                "countryname": "United States",
                "phoneCode": "1"
            },
            {
                "countrycode": "UY ",
                "countryflag": "https://restcountries.eu/data/ury.svg",

                "countryname": "Uruguay",
                "phoneCode": "598"
            },
            {
                "countrycode": "UZ ",
                "countryflag": "https://restcountries.eu/data/uzb.svg",

                "countryname": "Uzbekistan",
                "phoneCode": "998"
            },
            {
                "countrycode": "VU ",
                "countryflag": "https://restcountries.eu/data/vut.svg",

                "countryname": "Vanuatu",
                "phoneCode": "678"
            },
            {
                "countrycode": "VA ",
                "countryflag": "https://restcountries.eu/data/vat.svg",

                "countryname": "Vatican",
                "phoneCode": "379"
            },
            {
                "countrycode": "VE ",
                "countryflag": "https://restcountries.eu/data/ven.svg",

                "countryname": "Venezuela",
                "phoneCode": "58"
            },
            {
                "countrycode": "VN ",
                "countryflag": "https://restcountries.eu/data/vnm.svg",

                "countryname": "Vietnam",
                "phoneCode": "84"
            },
            {
                "countrycode": "WF ",
                "countryflag": "https://restcountries.eu/data/wlf.svg",

                "countryname": "Wallis and Futuna",
                "phoneCode": "681"
            },
            {
                "countrycode": "EH ",
                "countryflag": "https://restcountries.eu/data/esh.svg",

                "countryname": "Western Sahara",
                "phoneCode": "212"
            },
            {
                "countrycode": "YE ",
                "countryflag": "https://restcountries.eu/data/yem.svg",

                "countryname": "Yemen",
                "phoneCode": "967"
            },
            {
                "countrycode": "ZM ",
                "countryflag": "https://restcountries.eu/data/zmb.svg",

                "countryname": "Zambia",
                "phoneCode": "260"
            },
            {
                "countrycode": "ZW ",
                "countryflag": "https://restcountries.eu/data/zwe.svg",

                "countryname": "Zimbabwe",
                "phoneCode": "263"
            }
        ]
    }
}